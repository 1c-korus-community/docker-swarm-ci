# Материалы ко второй встрече 1С:KORUS Сommunity от 15.06.2022

Запись встречи на [YouTube](https://www.youtube.com/watch?v=lRGI_fol_bo)

Репозиторий содержит минимально необходимый набор файлов для построения контура автотестирования 1С с помощью Jenkins + Docker Swarm mode, [jenkins-lib](https://github.com/firstBitMarksistskaya/jenkins-lib) и [onec-docker](https://github.com/firstBitMarksistskaya/onec-docker)

Файлы в каталоге config нельзя использовать как есть, потребуется как минимум заменить имена\адреса хостов.

Доп. материалы:

- [статья на Инфостарте](https://infostart.ru/1c/articles/1681427/) по итогам доклада на IE 2021 Moscow Premiere ([видео](https://infostart.ru/video/w1681527/))
